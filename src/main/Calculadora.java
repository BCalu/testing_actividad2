package main;

public class Calculadora {
	
	public static int Sumar(int num1, int num2) {
		return num1 + num2;
	}
	
	public static int Restar(int num1, int num2) {
		return num1 - num2;
	}
	
	public static int Multiplicar(int num1, int num2) {
		return num1 * num2;
	}
	
	public static int Dividir(int num1, int num2) {
		return num1 / num2;
	}
}
