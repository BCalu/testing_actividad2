package main;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculadoraTest {

	//Casos de prueba Sumar
	@Test
	void SumarPositivos() {
		int resultado = Calculadora.Sumar(1,1);
		int esperado = 2;
		assertEquals(esperado, resultado);
	}

	@Test
	void SumarNegativos() {
		int resultado = Calculadora.Sumar(-1, -1);
		int esperado = -2;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void SumarPositivoNegativo() {
		int resultado = Calculadora.Sumar(-1, 3);
		int esperado = 2;
		assertEquals(esperado, resultado);
	}
	
	
	//Casos de prueba Restar
	@Test
	void RestarPositivos() {
		int resultado = Calculadora.Restar(3, 1);
		int esperado = 2;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void RestarNegativos() {
		int resultado = Calculadora.Restar(-3, -1);
		int esperado = -2;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void RestarPositivosNegativos() {
		int resultado = Calculadora.Restar(3, -1);
		int esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	
	//Casos de prueba Multiplicar
	@Test
	void MultiplicarPositivos() {
		int resultado = Calculadora.Multiplicar(3, 2);
		int esperado = 6;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void MultiplicarNegativos() {
		int resultado = Calculadora.Multiplicar(-3, -1);
		int esperado = 3;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void MultiplicarPositivosNegativos() {
		int resultado = Calculadora.Multiplicar(3, -1);
		int esperado = -3;
		assertEquals(esperado, resultado);
	}
	
	
	//Casos de prueba Dividir
	@Test
	void DividirPositivos() {
		int resultado = Calculadora.Dividir(6, 2);
		int esperado = 3;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void DividirNegativos() {
		int resultado = Calculadora.Dividir(-6, -2);
		int esperado = 3;
		assertEquals(esperado, resultado);
	}
	
	@Test
	void DividirPositivosNegativos() {
		int resultado = Calculadora.Dividir(6, -2);
		int esperado = -3;
		assertEquals(esperado, resultado);
	}
}
